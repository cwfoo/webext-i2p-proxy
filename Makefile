.POSIX:

source_files=	main.js manifest.json options.html options.js LICENSE README.md

all: _build/i2p-proxy.xpi

_build/i2p-proxy.xpi: ${source_files}
	@mkdir -p _build/
	zip -r -FS $@ ${source_files}

clean:
	rm -rf _build/
