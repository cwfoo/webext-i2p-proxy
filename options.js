function saveOptions(event) {
    browser.storage.local.set({
        proxyHost: document.getElementById("proxy-host").value.trim(),
        proxyPort: document.getElementById("proxy-port").value.trim()
    });
    event.preventDefault();
}

function restoreOptions() {
    browser.storage.local.get(data => {
        document.getElementById("proxy-host").value = data.proxyHost;
        document.getElementById("proxy-port").value = data.proxyPort;
    });
}

document.querySelector("form").addEventListener("submit", saveOptions);
document.addEventListener("DOMContentLoaded", restoreOptions);
