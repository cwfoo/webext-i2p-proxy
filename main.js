let proxyHost = "127.0.0.1";
let proxyPort = 4444;

// Set default preferences on installation.
browser.runtime.onInstalled.addListener(() => {
    browser.storage.local.set({
        proxyHost: proxyHost,
        proxyPort: proxyPort
    });
});

// Get user preferences.
browser.storage.local.get(data => {
    proxyHost = data.proxyHost;
    proxyPort = data.proxyPort;
});
browser.storage.onChanged.addListener(changeData => {
    proxyHost = changeData.proxyHost.newValue;
    proxyPort = changeData.proxyPort.newValue;
});

browser.proxy.onRequest.addListener(
    () => ({type: "http", host: proxyHost, port: proxyPort}),
    {urls: ["*://*.i2p/*"]}
);
