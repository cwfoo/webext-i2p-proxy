# webext-i2p-proxy
Firefox extension that proxies I2P URLs to an I2P router.


## Development
Run `make` to build `_build/i2p-router.xpi`.


## License
This project is distributed under the terms of the BSD 2-Clause License (see
[LICENSE](./LICENSE)).


## Contributing
Bug reports, suggestions, and patches should be submitted on Codeberg:
https://codeberg.org/cwfoo/webext-i2p-proxy
